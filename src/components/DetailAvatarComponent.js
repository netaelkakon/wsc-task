import React, { Component } from 'react';

class DetailAvatarComponent extends Component {
    constructor(props) {
        super(props);
    }

    getColor(deep) {
        let h = 200, s = 65, l = 35;
        const deepColor = (color => {
            return Math.min(100, color);
        });
        const sigmoidLevel = (color => {
            const factor = 1/(1+Math.pow(7,-deep)) - 0.5;
            return color + ((100 - color) * factor);
        });
        const factor = 8 * Math.log(deep + 1);
        return `hsl(${h}, ${s + deepColor(deep * factor/2)}%, ${l + deepColor(deep * factor)}%)`;
      }

    render() {
        return (
            <svg width="30" height="30">
              <rect x="0" y="0" width="30" height="30"  style={{fill: this.getColor(this.props.deep)}}/>
            </svg>
        );
      }
}

export default DetailAvatarComponent;