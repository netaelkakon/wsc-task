class Requester {
    constructor(url) {
        this.baseUrl = url;
    }

    get(path) {
        return fetch(`${this.baseUrl}/${path}`).then(res => res.json());
    }

    post(path, form) {
        return fetch(`${this.baseUrl}/${path}`, {method: "POST", body: JSON.stringify(form)});
    }

    getAll() {
        return this.get('player/all');
    }

    getPlayer(id) {
        return this.get(`player/${id}`);
    }

    postPlayer(id, form) {
        return this.post(`player/${id}`, form);
    }
    
}

const url = 'https://nodesenior.azurewebsites.net';
const instance = new Requester(url);
Object.freeze(instance);

 module.exports = instance;