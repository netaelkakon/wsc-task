import React, { Component } from 'react';
import Requester from '../../services/Requester.js';
import ObjectDetailComponent from '../ObjectDetailComponent';
import detail from './PlayerComponentStyles.css';

class PlayerComponent extends Component {
    constructor(props) {
        super(props);
        this.savePlayer = this.savePlayer.bind(this);
        this.fetchPlayer = this.fetchPlayer.bind(this);
        this.fetchDelay = 1000;
        this.firstFetch = true;
    }

    fetchPlayer() {
        Requester.getAll().then(res => {
            res.length && Requester.getPlayer(res[0]).then(playerDetails => {
                if (this.firstFetch) {
                    this.setState({details: playerDetails, curDetails: playerDetails, name: res[0]});
                    this.firstFetch = false;
                } else {
                    this.setState({curDetails: playerDetails, name: res[0]});
                }
                
            });
        });
    }

    componentDidMount() {
        this.fetchPlayer();
        setInterval(this.fetchPlayer, this.fetchDelay);
    }

    savePlayer() {
        Requester.postPlayer(this.state.details.id, this.state.details).then(res => console.log('save res:', res));
    }

    render() {
        return(
            <div>
                {this.state && <ObjectDetailComponent details={this.state.curDetails}
                name={this.state.name} deep={0}></ObjectDetailComponent>}
                {this.state && <button className="player-save-btn" onClick={this.savePlayer}>SAVE</button>}
            </div>
        );
    }
}

export default PlayerComponent;