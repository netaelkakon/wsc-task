import React, { Component } from 'react';
import DetailComponent from '../DetailComponent';
import styles from './StringDetailComponentStyles.css';

class StringDetailComponent extends Component {
    constructor(props) {
        super(props);
        this.valueChanged = this.valueChanged.bind(this);
        this.renderMedia = this.renderMedia.bind(this);
        // basic patterns for url, image and video:
        this.urlPatt = new RegExp("^https?://");
        this.imgPatt = new RegExp("\.(jpg|png|gif|bmp)$");
        this.vidPatt = new RegExp("\.(mp4|ogg|webm)$");
    }

    isUrl(path) {
        return this.urlPatt.test(path.toLowerCase());
    }

    isImage(path) {
        return this.imgPatt.test(path.toLowerCase());
    }

    isVideo(path) {
        return this.vidPatt.test(path.toLowerCase());
    }

    renderMedia(path) {
        if (!this.isUrl(path)) {
            return;
        }
        if (this.isImage(path)) {
            return (
                <img src={path} className="media-content"/>
            );
        }
        if (this.isVideo(path)) {
            return (
                <video src={path}  className="media-content" controls/>
            );
        }
    }

    valueChanged(value) {
        this.props.onDetailChange(this.props.name, value);
    }

    render() {
        return(
            <div>
                <DetailComponent name={this.props.name} value={this.props.value} onDetailChange={this.valueChanged} type="string" deep={this.props.deep}></DetailComponent>
                {this.renderMedia(this.props.value)}
            </div>
        );
    }
}

export default StringDetailComponent;