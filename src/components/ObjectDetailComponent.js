import React, { Component } from 'react';
import DetailComponent from './DetailComponent';
import DetailAvatarComponent from './DetailAvatarComponent';
import StringDetailComponent from './string-detail/StringDetailComponent';
import NumberDetailComponent from './NumberDetailComponent';
import detail from './styles.css';

class ObjectDetailComponent extends Component {
    constructor(props) {
        super(props);
        this.updateDetailValue = this.updateDetailValue.bind(this);
        this.getDetailValue = this.getDetailValue.bind(this);
        this.state = {details: props.details};
    }

    /**
     * get detail to render by type
     */
    getDetailValue(detail, detailName) {
        let detailValue = null;
        switch (typeof detail) {
            case ('object'):
                detailValue = <ObjectDetailComponent details={detail} name={detailName} key={detailName} deep={this.props.deep + 1}></ObjectDetailComponent>
                break;
            case ('number'):
                detailValue = <NumberDetailComponent onDetailChange={this.updateDetailValue} name={detailName} value={detail} key={detailName} deep={this.props.deep + 1}></NumberDetailComponent>
                break;
            case ('string'):
                detailValue = <StringDetailComponent onDetailChange={this.updateDetailValue} name={detailName} value={detail} key={detailName} deep={this.props.deep + 1}></StringDetailComponent>
                break;
            default:
            break;
        }
        return detailValue;
    }

    updateDetailValue(name, value) {
        const updatedDetails = this.state.details;
        updatedDetails[name] = value;
        this.setState({details: updatedDetails});
    }

    render() {
        return(
            <div style={{marginLeft: this.props.deep ? '2em' : '0'}}>
                <div className="detail-holder">
                    <DetailAvatarComponent deep={this.props.deep}></DetailAvatarComponent>
                    <span style={{margin: '0 4px'}}>{this.props.name}</span>
                </div>
                <div>
                {Object.keys(this.props.details).map(detail => this.getDetailValue(this.props.details[detail], detail))}
                </div>
            </div>
            
        );
    }
}

export default ObjectDetailComponent;