import React, { Component } from 'react';
import DetailAvatarComponent from './DetailAvatarComponent';
import detail from './styles.css';

class DetailComponent extends Component {
  constructor(props) {
    super(props);
    this.inputChanged = this.inputChanged.bind(this);
    this.updateDetailValue = this.updateDetailValue.bind(this);
    this.state = {dirty: false, value: props.value};
  }

  componentWillReceiveProps(nextProps) {
    // update value if no changes made by user:
    if (this.state.value !== nextProps.value && !this.state.dirty) {
      this.setState({value: nextProps.value});
      this.updateDetailValue(nextProps.value);
    }
  }

  inputChanged(e) {
    this.setState({dirty: true, value: e.target.value});
    this.updateDetailValue(e.target.value);
  }

  updateDetailValue(value) {
    this.props.onDetailChange(value);
  }

  render() {
    return (
      <div className="detail-holder" style={{marginLeft: '2em'}}>
        <DetailAvatarComponent deep={this.props.deep}></DetailAvatarComponent>
        <span style={{margin: '0 0.5em'}}>{this.props.name}</span>
        <input type={this.props.type} value={this.state.value} onChange={this.inputChanged} style={{width: this.props.type === 'number' ? '5em' : ''}}/>
      </div>
    );
  }
}
  
export default DetailComponent;