import React, { Component } from 'react';
import DetailComponent from './DetailComponent';

class NumberDetailComponent extends Component {
    constructor(props) {
        super(props);
        this.valueChanged = this.valueChanged.bind(this);
    }

    valueChanged(value) {
        this.props.onDetailChange(this.props.name, value);
    }

    render() {
        return(
            <DetailComponent name={this.props.name} value={this.props.value} onDetailChange={this.valueChanged} type="number" deep={this.props.deep}></DetailComponent>
        );
    }
}

export default NumberDetailComponent;